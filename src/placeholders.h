/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "outfmt.h"

int placeholders_setup(ofmt_t);

/* This struct collects additional information that gets copied in the
 * opaque structure within a ofmt_t object.  This information is
 * ultimately available to the placeholder resolution functions
 * defined in 'placeholders.c'. */
typedef struct {
    unsigned incremental_id;
    const char *feed_title;
    const char *feed_identifier;
} placeholder_extra_t;

void placeholders_set_extra(ofmt_t, const placeholder_extra_t *extra);
