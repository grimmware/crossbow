/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "placeholders.h"

#include <err.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>

#include <mrss.h>

struct state {
    placeholder_extra_t extra;
    int buflen;
    char bytes[];
};

typedef struct state state_t;

#define RESOLVE_ITEM_STR(field) \
    static const char * resolve_ ## field (void *item, void *opaque)    \
    {                                                                   \
        return ((mrss_item_t *)item)->field;                            \
    }                                                                   \

RESOLVE_ITEM_STR(title);
RESOLVE_ITEM_STR(title_type);
RESOLVE_ITEM_STR(link);
RESOLVE_ITEM_STR(description_type);
RESOLVE_ITEM_STR(copyright);
RESOLVE_ITEM_STR(copyright_type);
RESOLVE_ITEM_STR(author);
RESOLVE_ITEM_STR(author_uri);
RESOLVE_ITEM_STR(author_email);
RESOLVE_ITEM_STR(contributor);
RESOLVE_ITEM_STR(contributor_uri);
RESOLVE_ITEM_STR(contributor_email);
RESOLVE_ITEM_STR(comments);
RESOLVE_ITEM_STR(pubDate);
RESOLVE_ITEM_STR(guid);
RESOLVE_ITEM_STR(source);
RESOLVE_ITEM_STR(source_url);
RESOLVE_ITEM_STR(enclosure);
RESOLVE_ITEM_STR(enclosure_url);
RESOLVE_ITEM_STR(enclosure_type);
RESOLVE_ITEM_STR(description);

#define RESOLVE_ITEM_INT(field) \
    static const char * resolve_ ## field (void *item, void *opaque)    \
    {                                                                   \
        state_t *state = opaque;                                        \
        int n;                                                          \
                                                                        \
        n = snprintf(state->bytes, state->buflen, "%d",                 \
                     ((mrss_item_t *)item)->field);                     \
                                                                        \
        if (n >= state->buflen)                                         \
             errx(EX_SOFTWARE, "not enough buffer");                    \
                                                                        \
        return state->bytes;                                            \
    }                                                                   \

RESOLVE_ITEM_INT(guid_isPermaLink);
RESOLVE_ITEM_INT(enclosure_length);

static const char * resolve_incremental_id(void *item, void *opaque)
{
    state_t *state = opaque;
    int n;

    n = snprintf(state->bytes, state->buflen, "%06u",
                 state->extra.incremental_id);

    if (n >= state->buflen)
         errx(EX_SOFTWARE, "not enough buffer");

    return state->bytes;
}

#define RESOLVE_FEED_ITEM_STR(field) \
    static const char * resolve_ ## field (void *item, void *opaque)    \
    {                                                                   \
        state_t *state = opaque;                                        \
        return state->extra.field;                                      \
    }

RESOLVE_FEED_ITEM_STR(feed_title);
RESOLVE_FEED_ITEM_STR(feed_identifier);

int placeholders_setup(ofmt_t ofmt)
{
    int buflen;
    state_t *state;

    buflen = 1 + snprintf(NULL, 0, "%d", INT_MAX);

    state = malloc(sizeof(struct state) + buflen);
    if (!state) {
        warn("malloc failed");
        return -1;
    }
    *state = (state_t){ .buflen = buflen };
    ofmt_set_opaque(ofmt, state, free);

    #define PLACEHOLDER(key, field, ...) \
        if (ofmt_set_resolver(ofmt, key, resolve_ ## field) == -1) \
            goto fail;
    #include "placeholders_gen.h"
    #undef PLACEHOLDER

    if (ofmt_set_pipe_resolver(ofmt, resolve_description) == -1)
        goto fail;

    return 0;

fail:
    warnx("placeholders_setup failed");
    ofmt_print_error(ofmt);
    return -1;
}

void placeholders_set_extra(ofmt_t ofmt, const placeholder_extra_t *extra)
{
    state_t *state = ofmt_get_opaque(ofmt);
    if (!state)
        errx(EX_SOFTWARE,
            "placeholders_set_extra used before placeholders_setup?"
        );

    state->extra = *extra;
}
