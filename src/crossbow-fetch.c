/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <err.h>
#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

#include <mrss.h>
#include <uthash.h>

#include "config.h"
#include "hash.h"
#include "help_helper.h"
#include "logging.h"
#include "outfmt.h"
#include "placeholders.h"
#include "savefile_aux.h"
#include "text.h"
#include "str.h"
#include "subcmd_common.h"

struct opts
{
    const char *uid;
    bool dry_mark : 1;
    bool dry_exec : 1;
    bool catch_up : 1;
};

struct fetch_ctx
{
    savefile_t *savefile;
    ofmt_t ofmt;
    bool ofmt_loaded;
};

struct process_ctx
{
    feed_t *feed;
    mrss_item_t *item;
    str_t *item_uid;
    const struct opts *opts;

    struct {
        unsigned *incremental_id;
        const char *feed_title;
        const char *feed_identifier;
    } extra_pholders;
};

typedef struct opts opts_t;
typedef struct fetch_ctx fetch_ctx_t;
typedef struct process_ctx process_ctx_t;

static const help_flag_t flags[] = {
    {
        .optname = "-i",
        .argname = "ID",
        .description="only fetch the given identifier"
    }, {
        .optname = "-c",
        .description="catch up feeds, skip processing entirely",
    }, {
        .optname = "-d",
        .description="do not mark items as seen",
    }, {
        .optname = "-D",
        .description="do not run subprocesses",
    }, {}
};

static const struct help help = {
    .progname = "crossbow-fetch",
    .flags = flags,
};

static inline void usage(const char *progname, const char *warning)
{
    if (warning)
        warnx("%s", warning);
    fprintf(stderr, "Usage: %s [OPTIONS]\n", progname);
}

static opts_t read_opts(int argc, char **argv)
{
    int opt;

    opts_t result = {};

    while (opt = getopt(argc, argv, "cDdi:vh"), opt != -1)
        switch (opt) {
        case 'c':
            result.catch_up = true;
            break;

        case 'D':
            result.dry_exec = true;
            break;

        case 'd':
            result.dry_mark = true;
            break;

        case 'i':
            result.uid = optarg;
            break;

        case 'h':
            show_help(help_full, &help, NULL);

        case 'v':
            g_verbosity_level++;
            break;

        default:
            usage(argv[0], NULL);
            exit(EXIT_FAILURE);
        }

    if (!result.uid && optind < argc)
        result.uid = argv[optind++];

    for (int i = optind; i < argc; ++i)
        warnx("ignoring argument: %s", argv[i]);

    return result;
}

static void rss_show_error(const str_t *address, mrss_error_t e, CURLcode code)
{
    const char *reason;
    const char *kind;

    switch (e) {
        case MRSS_OK:
            assert(0);
            return;
        case MRSS_ERR_DOWNLOAD:
            reason = mrss_curl_strerror(code);
            kind = "download";
            break;
        case MRSS_ERR_POSIX:
            reason = strerror(errno);
            kind = "errno";
            break;
        default:
            reason = mrss_strerror(e);
            kind = "mrss";
    }
    warnx("cannot open feed [%.*s]: error_code=%d (%s: %s)",
          STR_FMT(address), e, kind, reason);
}

static mrss_t *rss_open(const feed_t *feed)
{
    mrss_t *rss = NULL;
    mrss_error_t error;
    CURLcode code = 0;
    char nul_term_url[512];

    str_t url = feed_get_provided_url(feed);
    feed_url_type_t urltype = feed_get_url_type(feed);

    say("feed [%s]", feed_get_name(feed));
    whisper(" opening address [%.*s] (%s)",
            STR_FMT(&url),
            feed_url_type_to_str(urltype));

    if (str_to_cstr(&url, nul_term_url, sizeof(nul_term_url)) == NULL) {
        warnx("feed url is too long (max %zu bytes)", sizeof(nul_term_url));
        return NULL;
    }

    switch (urltype) {
    case feed_ut_remote:
        error = mrss_parse_url_with_options_and_error(
            nul_term_url,
            &rss,
            NULL,
            &code
        );
        break;
    case feed_ut_local:
        error = mrss_parse_file(nul_term_url, &rss);
        break;
    case feed_ut_unknown:
        error = 0;
        warnx("Unknown URL type for \"%s\"", nul_term_url);
        break;
    default:
        errx(EX_SOFTWARE, "unrecognized in feed_url_type_t: %d", urltype);
    }

    if (error)
        rss_show_error(&url, error, code);

    return rss;
}

static int load_seen(const feed_t *feed, hash_t *seen)
{
    say(" loading items from savefile");

    feed_items_t items = feed_get_items(feed);
    for (unsigned i = 0; i < items.n_items; ++i) {
        whisper("  known item [%.*s]", STR_FMT(&items.items[i]));
        if (hash_insert(seen, &STR_ALIAS(&items.items[i]), NULL) == -1)
            return -1;
    }
    whisper(" done loading");

    return 0;
}

static int process_item_print(const process_ctx_t *pc)
{
    char buffer[256];
    mrss_item_t *item = pc->item;

    printf("ITEM: %.*s\n", STR_FMT(pc->item_uid));
    printf(" incremental_id: %u", *(pc->extra_pholders.incremental_id));

    #define print_if(fmt, item) if (item) printf(fmt, item)
    print_if(" title: %s\n", item->title);
    print_if(" title_type: %s\n", item->title_type);
    print_if(" link: %s\n", item->link);
    print_if(" description: %s\n", text_short(
        buffer,
        sizeof(buffer),
        item->description,
        NULL
    ));
    print_if(" description_type: %s\n", item->description_type);
    print_if(" copyright: %s\n", item->copyright);
    print_if(" copyright_type: %s\n", item->copyright_type);
    print_if(" author: %s\n", item->author);
    print_if(" author_url: %s\n", item->author_uri);
    print_if(" author_email: %s\n", item->author_email);
    print_if(" contributor: %s\n", item->contributor);
    print_if(" contributor_uri: %s\n", item->contributor_uri);
    print_if(" contributor_email: %s\n", item->contributor_email);
    print_if(" comments: %s\n", item->comments);
    print_if(" pubDate: %s\n", item->pubDate);
    print_if(" guid: %s\n", item->guid);
    print_if(" guid_isPermaLink: %d\n", item->guid_isPermaLink);
    print_if(" source: %s\n", item->source);
    print_if(" source_url: %s\n", item->source_url);
    print_if(" enclosure: %s\n", item->enclosure);
    print_if(" enclosure_url: %s\n", item->enclosure_url);
    print_if(" enclosure_length: %d\n", item->enclosure_length);
    print_if(" enclosure_type: %s\n", item->enclosure_type);
    #undef print_if

    return 0;   /* never fails */
}

static inline int process_item_ofmt(const fetch_ctx_t *fc,
                                    const process_ctx_t *pc)
{
    ofmt_evaluate_params_t params = {
        .item = (void *)pc->item,
        .dry_run = pc->opts->dry_exec,
    };
    char path[PATH_MAX];
    str_t subproc_chdir = feed_get_subproc_chdir(pc->feed);

    if (subproc_chdir.len > 0) {
        if (str_to_cstr(&subproc_chdir, path, sizeof(path)) == NULL) {
            warnx("cannot chdir to \"%.*s\": path too long",
                  STR_FMT(&subproc_chdir));
            return -1;
        }
        params.opt_subproc_chdir = path;
    }

    placeholders_set_extra(fc->ofmt, &(const placeholder_extra_t){
        .incremental_id = *(pc->extra_pholders.incremental_id),
        .feed_identifier = pc->extra_pholders.feed_identifier,
        .feed_title = pc->extra_pholders.feed_title,
    });

    if (ofmt_evaluate(fc->ofmt, &params) == 0)
        return 0;

    if (whisper_enabled)
        ofmt_print_error(fc->ofmt);

    return -1;
}

static int process_item(const fetch_ctx_t *fc,
                        const process_ctx_t *pc)
{
    int e = fc->ofmt_loaded
        ? process_item_ofmt(fc, pc)
        : process_item_print(pc);

    if (e)
        warnx("failure while fetching feed \"%s\"",
            feed_get_name(pc->feed)
        );

    /* Note: the value is purposely incremented even if the processing
     * failed. */
    ++(*(pc->extra_pholders.incremental_id));

    return e;
}

static void load_outfmt(fetch_ctx_t *fc, const feed_t *feed)
{
    bool ofmt_loaded = false;
    ofmt_mode_t om;

    str_t spec = feed_get_outfmt(feed);
    if (!spec.bytes)
        goto exit;

    if (feed_get_output_mode(feed) == feed_om_print)
        goto exit;

    if (convert_out_mode(feed_get_output_mode(feed), &om) == -1)
        goto exit;

    if (ofmt_compile(fc->ofmt, om, spec.bytes, spec.len) == 0) {
        ofmt_loaded = true;
    } else {
        warnx("unable to compile format specification \"%.*s\"",
              STR_FMT(&spec));
        ofmt_print_error(fc->ofmt);
        warnx("using default output format");
    }

exit:
    fc->ofmt_loaded = ofmt_loaded;
}

static int feed_fetch(const fetch_ctx_t *fc, feed_t *feed,
                      const opts_t *opts)
{
    /* fetch and process the given feed.
     *
     * Returns -1 on critical error (no more feeds are processed).
     * Returns 0 if all items were processed successfully.
     * Returns >0 to indicate the number of non-critical failures.
     */

    int result = 0;
    hash_t seen_items = HASH_INIT;
    str_t store_items_buffer[feed_max_items] = {};
    feed_items_t store_items = {
        .items = store_items_buffer,
    };

    mrss_t *mrss = rss_open(feed);
    if (!mrss) {
        result++;
        goto exit;
    }

    if (load_seen(feed, &seen_items) == -1) {
        result = -1;
        goto exit;
    }

    whisper(" processing feed");

    unsigned incremental_id = feed_get_items_count(feed);
    for (mrss_item_t *item = mrss->item; item; item = item->next) {
        str_t uid = STR_CAST(item->guid ?: item->link);
        if (!uid.bytes) {
            warnx("  ignored item, bad feed missing both guid and link");
            continue;
        }

        if (hash_contains(&seen_items, &uid))
            whisper("  item [%.*s] seen before", STR_FMT(&uid));
        else {
            whisper("  item [%.*s] is new", STR_FMT(&uid));

            if (opts->catch_up) {
                whisper("  marking item as seen (catch-up mode)");
                ++incremental_id;
            } else {
                process_ctx_t process_ctx = {
                    .feed = feed,
                    .item = item,
                    .item_uid = &uid,
                    .opts = opts,
                    .extra_pholders = {
                        .incremental_id = &incremental_id,
                        .feed_title = mrss->title,
                        .feed_identifier = feed_get_name(feed),
                    },
                };

                if (process_item(fc, &process_ctx) == -1) {
                    whisper("  processing failed, not marked as seen");
                    ++result;
                    continue;
                }
            }
        }

        store_items_buffer[store_items.n_items++] = uid;
    }

    if (incremental_id > feed_get_items_count(feed)) {
        feed_set_items_count(feed, incremental_id);

        /* having a higher incremental id means that at least one new item
         * was emitted, possibly with success */
        if (feed_set_items(feed, &store_items))
            ++result;
    }

    whisper(" done processing feed");

exit:
    hash_free(&seen_items);
    mrss_free(mrss);

    if (opts->dry_mark)
        whisper("-d mode: updates not persisted");
    else if (feed_persist(feed) == -1) {
        warnx("unable to persist feed");
        ++result;
    }

    return result;
}

static int fetch(fetch_ctx_t *fc, const opts_t *opts)
{
    feed_t *feed;
    int failed_feeds_count = 0;
    void *aux = NULL;

    if (opts->uid)
        savefile_get_feed(fc->savefile, opts->uid);
    else if (savefile_load_all(fc->savefile) == -1) {
        warnx("faulty savefile load");
        return -1;
    }

    while (feed = savefile_iter_feeds(fc->savefile, &aux), feed) {
        if (!opts->catch_up)
            load_outfmt(fc, feed);

        switch (feed_fetch(fc, feed, opts)) {
            case -1:
                return -1;
            case 0:
                break;
            default:
                ++failed_feeds_count;
        }
    }

    return failed_feeds_count;
}

int main(int argc, char **argv)
{
    int status = -1;
    fetch_ctx_t fc = {};

#ifdef HAVE_PLEDGE
    if (pledge("stdio rpath wpath cpath inet dns proc exec", NULL) == -1)
        err(1, "pledge");
#endif

    const opts_t opts = read_opts(argc, argv);

    fc.ofmt = ofmt_new();
    if (!fc.ofmt) {
        warn("ofmt_new");
        goto exit;
    }

    if (placeholders_setup(fc.ofmt) == -1)
        goto exit;

    fc.savefile = open_savefile(false);
    if (!fc.savefile) {
        warnx("open_savefile failed");
        goto exit;
    }

    status = fetch(&fc, &opts);

exit:
    savefile_free(fc.savefile);
    ofmt_del(fc.ofmt);
    return status ? EXIT_FAILURE : EXIT_SUCCESS;
}
