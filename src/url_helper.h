#pragma once

#include "savefile.h"
#include "str.h"

typedef struct {
    str_t url;
    feed_url_type_t type;
} url_pair_t;

int url_get_effective(const url_pair_t *provided, url_pair_t *effective);
