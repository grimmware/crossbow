/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <err.h>
#include <stdlib.h>
#include <unistd.h>

#include "config.h"
#include "help_helper.h"
#include "logging.h"
#include "savefile_aux.h"
#include "subcmd_common.h"

struct opts
{
    const char *uid;
};

typedef struct opts opts_t;

static const help_flag_t help_flags[] = {
    {
        .optname = "-i",
        .argname = "ID",
        .description = "feed identifier",
    }, {}
};

static const struct help help = {
    .progname = "crossbow-query",
    .flags = help_flags,
};

static inline void usage(const char *progname, const char *warning)
{
    if (warning)
        warnx("%s", warning);
    fprintf(stderr, "Usage: %s [OPTIONS]\n", progname);
}

static opts_t read_opts(int argc, char **argv)
{
    int opt;

    opts_t result = {};

    while (opt = getopt(argc, argv, "i:vh"), opt != -1)
        switch (opt) {
        case 'i':
            result.uid = optarg;
            break;

        case 'h':
            show_help(help_full, &help, NULL);
            break;

        case 'v':
            g_verbosity_level++;
            break;

        default:
            usage(argv[0], NULL);
            exit(EXIT_FAILURE);
        }

    if (!result.uid && optind < argc)
        result.uid = argv[optind++];

    for (int i = optind; i < argc; ++i)
        warnx("ignoring argument: %s", argv[i]);

    return result;
}

static void show_items(const feed_t *feed)
{
    if (g_verbosity_level == 0)
        return;

    feed_items_t items = feed_get_items(feed);

    printf(" items count: %u\n", items.n_items);
    if (g_verbosity_level < 2)
        return;

    printf(" next incremental item id: %u\n", feed_get_items_count(feed));
    for (unsigned i = 0; i < items.n_items; ++i)
        printf(" item guid: %.*s\n", STR_FMT(&items.items[i]));
}

static void show_feed_details(const feed_t *feed)
{
    if (g_verbosity_level == 0)
        return;

    str_t s;
    feed_output_mode_t out_mode;

    printf(" url type: %s\n", feed_url_type_to_str(feed_get_url_type(feed)));

    s = feed_get_effective_url(feed);
    printf(" effective url: %.*s\n", STR_FMT(&s));

    out_mode = feed_get_output_mode(feed);
    printf(" output mode: %s\n", feed_output_mode_to_str(out_mode));

    s = feed_get_outfmt(feed);
    printf(
        " output format: \"%.*s\"%s\n",
        STR_FMT(&s),
        out_mode == feed_om_print
                ? " (ignored in this output mode)"
                : ""
    );

    s = feed_get_subproc_chdir(feed);
    printf(
        " subprocess chdir: \"%.*s\"%s\n",
        STR_FMT(&s),
        out_mode != feed_om_subproc && out_mode != feed_om_pipe
                ? " (ignored in this output mode)"
                : ""
    );
}

static int show_feeds(const savefile_t *savefile)
{
    const feed_t *feed;
    void *aux = NULL;
    int err = 0;

    while (feed = savefile_citer_feeds(savefile, &aux), feed) {
        str_t provided_url = feed_get_provided_url(feed);

        printf("%s %.*s\n", feed_get_name(feed), STR_FMT(&provided_url));

        if (!feed_exists(feed)) {
            printf(" feed does not exist\n");
            err = -1;
            continue;
        }

        show_feed_details(feed);
        show_items(feed);
    }

    return err;
}

int main(int argc, char **argv)
{
    savefile_t *savefile;
    opts_t opts;

#ifdef HAVE_PLEDGE
    if (pledge("stdio rpath", NULL) == -1)
        err(1, "pledge");
#endif

    opts = read_opts(argc, argv);

    savefile = open_savefile(false);
    if (!savefile)
        errx(EXIT_FAILURE, "open_savefile failed");

    if (opts.uid)
        /* using savefile_get_feed we add a feed_t object the savefile,
         * which will be seen by show_feeds, even in case the
         * corresponding file does not exist. */
        savefile_get_feed(savefile, opts.uid);
    else if (savefile_load_all(savefile) == -1)
        errx(EXIT_FAILURE, "faulty savefile load");

    int err = show_feeds(savefile);
    savefile_free(savefile);
    return err == 0
           ? EXIT_SUCCESS
           : EXIT_FAILURE;
}
