/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* These placeholders correspond to the fields of mrss_item_t */
PLACEHOLDER("a",  author)
PLACEHOLDER("am", author_email)
PLACEHOLDER("au", author_uri)
PLACEHOLDER("cm", comments)
PLACEHOLDER("co", contributor)
PLACEHOLDER("cm", contributor_email)
PLACEHOLDER("cu", contributor_uri)
PLACEHOLDER("cr", copyright)
PLACEHOLDER("ct", copyright_type)
PLACEHOLDER("d",  description)
PLACEHOLDER("dt", description_type)
PLACEHOLDER("en", enclosure)
PLACEHOLDER("el", enclosure_length)
PLACEHOLDER("et", enclosure_type)
PLACEHOLDER("eu", enclosure_url)
PLACEHOLDER("g",  guid)
PLACEHOLDER("gp", guid_isPermaLink)
PLACEHOLDER("l",  link)
PLACEHOLDER("pd", pubDate)
PLACEHOLDER("sr", source)
PLACEHOLDER("su", source_url)
PLACEHOLDER("t",  title)
PLACEHOLDER("tt", title_type)

/* These placeholders correspond to extra fields in placeholder_extra_t */
PLACEHOLDER("n",  incremental_id)
PLACEHOLDER("ft", feed_title)
PLACEHOLDER("fi", feed_identifier)
