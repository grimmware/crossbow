/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "launcher.h"

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sysexits.h>
#include <unistd.h>

typedef enum {
    rm_simple,
    rm_pipe,
} run_mode_t;

enum {
    pipe_read_end = 0,
    pipe_write_end = 1,
};

static void inject_input(pid_t pid, int writefd, void *input, size_t len)
{
    struct sigaction oldact;
    const char *bytes = input;
    ssize_t n;

    if (sigaction(SIGPIPE,
                  &(struct sigaction){ .sa_handler = SIG_IGN },
                  &oldact))
    {
        warn("sigaction (ignore sigpipe) failed");
        goto exit;
    }

    while (len > 0) {
        n = write(writefd, bytes, len);
        switch (n) {
        case -1:
            if (errno == EPIPE)
                goto exit;

            warn("failed to pipe stdin");
            warnx(" kill(%d, SIGKILL)", pid);
            if (kill(pid, SIGKILL) == -1)
                warn("unable to kill child %d", pid);
        case 0:
            goto exit;

        default:
            bytes += n;
            len -= n;
        }
    }

exit:
    if (close(writefd) == -1)
        warnx("parent close(%d) failed", writefd);
    if (sigaction(SIGPIPE, &oldact, NULL))
        warn("sigaction (restore sigpipe) failed");
}

static int child(const launcher_run_t *run, run_mode_t mode, int pipefd[2])
{
    if (run->chdir_to && chdir(run->chdir_to) == -1)
        err(1, "unable to chdir(%s)", run->chdir_to);

    if (mode == rm_pipe) {
        if (close(pipefd[pipe_write_end]) == -1)
            warn("child close(%d) failed", pipefd[pipe_write_end]);
        if (dup2(pipefd[pipe_read_end], 0) == -1)
            err(EX_OSERR, "child dup2(%d, 0) failed", pipefd[pipe_read_end]);
    } else if (mode == rm_simple) {
        if (close(0) == -1)
            err(EX_OSERR, "child close(0) failed");
        if (open("/dev/null", O_RDONLY) == -1)    /* replacing closed stdin */
            err(EX_OSERR, "child open /dev/null failed");
    } else
        err(EX_SOFTWARE, "unhandled case");

    execvp(run->argv[0], (char **)run->argv);
    err(EX_OSERR, "faulty execvp(\"%s\", ...)", run->argv[0]);
}

int launcher_execvp(const launcher_run_t *run, int *status)
{
    pid_t pid;
    int pipefd[2];

    run_mode_t mode = rm_simple;

    if (run->input.bytes && run->input.len) {
        if (pipe(pipefd) == -1) {
            warn("pipe failed");
            return -1;
        }

        mode = rm_pipe;
    }

    pid = fork();
    if (pid == -1) {
        warn("fork failed");
        return -1;
    }

    if (pid == 0)
        child(run, mode, pipefd);

    if (mode == rm_pipe) {
        if (close(pipefd[pipe_read_end]) == -1)
            warn("parent close(%d) failed", pipefd[pipe_read_end]);
        inject_input(
            pid,
            pipefd[pipe_write_end],
            run->input.bytes,
            run->input.len
        );
    }

    if (waitpid(pid, status, 0) == -1) {
        warn("waitpid %d failed", pid);
        return -1;
    }

    return 0;
}
