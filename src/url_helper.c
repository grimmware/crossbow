/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <err.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

#include "url_helper.h"

static bool has_supported_remote_protocol(const str_t *url)
{
    return str_cmp_up_to(url, &STR_DEFINE("http://"), 7) == 0
        || str_cmp_up_to(url, &STR_DEFINE("https://"), 8) == 0
        || str_cmp_up_to(url, &STR_DEFINE("gopher://"), 8) == 0
        ;
}

static bool obviously_local(const str_t *url)
{
    if (url->len == 0 || !url->bytes)
        return false;

    if (url->bytes[0] == '/' || url->bytes[0] == '.')
        return true;

    return false;
}

static feed_url_type_t detect_url_type(const str_t *url)
{
    if (str_cmp_up_to(url, &STR_DEFINE("file://"), 7) == 0)
        return feed_ut_local;

    if (obviously_local(url))
        return feed_ut_local;

    return feed_ut_remote;
}

static int add_https_prefix(str_t *dst, const str_t *src)
{
    unsigned len = src->len + sizeof("https://");   /* inclusive of '\0' */

    char *bytes = malloc(len);
    if (!bytes) {
        warn("malloc failed");
        return -1;
    }

    memcpy(bytes, "https://", sizeof("https://") - 1);
    memcpy(bytes + sizeof("https://") - 1, src->bytes, src->len);
    str_assign(dst, bytes, len - 1, true);

    return 0;
}

static int abspath(str_t *dst, const str_t *src)
{
    char path[PATH_MAX];
    const char *bytes;

    _Static_assert(PATH_MAX > sizeof(dst->len), "abspath cannot work");

    if (getcwd(path, sizeof(path)) == NULL) {
        warn("getcwd failed");
        return -1;
    }

    size_t i = 0, j = strlen(path);
    path[j++] = '/';
    while (i < src->len && j < sizeof(path) - 1)
        path[j++] = src->bytes[i++];
    path[j] = '\0';

    bytes = strndup(path, sizeof(path));
    if (!bytes) {
        warn("strndup failed");
        return -1;
    }

    *dst = (str_t){
        .bytes = bytes,
        .len = j,
        .owned = true,
    };

    return 0;
}

static int handle_local(const url_pair_t *query, url_pair_t *answer)
{
    str_t path;
    const str_t *q_url = &query->url;

    path = str_cmp_up_to(q_url, &STR_DEFINE("file://"), 7) == 0
         ? str_sub(q_url, 7, UINT_MAX)
         : STR_ALIAS(q_url);

    if (path.len == 0) {
        warnx("invalid empty path");
        return -1;
    }

    answer->type = feed_ut_local;
    return path.bytes[0] == '/'
        ? str_dup(&answer->url, &path)
        : abspath(&answer->url, &path);
}

static int handle_remote(const url_pair_t *query, url_pair_t *answer)
{
    const str_t *q_url = &query->url;

    answer->type = feed_ut_remote;
    return has_supported_remote_protocol(q_url)
         ? str_dup(&answer->url, q_url)
         : add_https_prefix(&answer->url, q_url);
}

static int handle_detect(const url_pair_t *query, url_pair_t *answer)
{
    switch (answer->type = detect_url_type(&query->url)) {
        case feed_ut_remote:
            return handle_remote(query, answer);
        case feed_ut_local:
            return handle_local(query, answer);
        default:
            break;
    }
    err(EX_SOFTWARE, "detect_url_type returns bogus");
}

int url_get_effective(const url_pair_t *query, url_pair_t *answer)
{
    *answer = (url_pair_t){};

    switch (query->type) {
        case feed_ut_unknown:
            return handle_detect(query, answer);
        case feed_ut_local:
            return handle_local(query, answer);
        case feed_ut_remote:
            return handle_remote(query, answer);
    }
    err(EX_SOFTWARE, "unhandled case? %d", query->type);
}
