/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <err.h>
#include <stdlib.h>
#include <unistd.h>

#include "config.h"
#include "logging.h"
#include "subcmd_common.h"
#include "help_helper.h"
#include "savefile_aux.h"

struct opts
{
    const char *uid;
};

typedef struct opts opts_t;

static const help_flag_t help_flags[] = {
    {
        .optname = "-i",
        .argname = "ID",
        .description = "feed identifier",
    }, {}
};

static const struct help help = {
    .progname = "crossbow-del",
    .flags = help_flags,
};

static opts_t read_opts(int argc, char **argv)
{
    int opt;

    opts_t result = {};

    while (opt = getopt(argc, argv, "hi:v"), opt != -1)
        switch (opt) {
        case 'h':
            show_help(help_full, &help, NULL);

        case 'i':
            result.uid = optarg;
            break;

        case 'v':
            g_verbosity_level++;
            break;

        default:
            show_help(help_usage, &help, NULL);
        }

    if (!result.uid) {
        if (optind < argc)
            result.uid = argv[optind++];
        else
            show_help(help_usage, &help, "missing mandatory identifier");
    }

    for (int i = optind; i < argc; ++i)
        warnx("ignoring argument: %s", argv[i]);

    return result;
}

int main(int argc, char **argv)
{
    opts_t opts;
    savefile_t *savefile;
    feed_t *feed;
    int exit_val = EXIT_FAILURE;

#ifdef HAVE_PLEDGE
    if (pledge("stdio rpath wpath cpath",  NULL) == -1)
        err(1, "pledge");
#endif

    opts = read_opts(argc, argv);
    savefile = open_savefile(false);
    if (!savefile) {
        warnx("open_savefile failed");
        goto exit;
    }

    feed = savefile_get_feed(savefile, opts.uid);
    if (!feed) {
        warnx("no feed named \"%s\"", opts.uid);
        goto exit;
    }

    feed_mark_deleted(feed);
    if (feed_persist(feed) != -1)
        exit_val = EXIT_SUCCESS;

exit:
    savefile_free(savefile);
    return exit_val;
}
