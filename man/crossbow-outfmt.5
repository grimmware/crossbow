.Dd July 11, 2020
.Dt CROSSBOW 5 URM
.Os \&
.\" ---------------------------------------------------------------------
.Sh NAME
.Nm crossbow-outfmt
.Nd Output format reference
.\" ---------------------------------------------------------------------
.Sh SYNOPSIS
.Nm crossbow outfmt
.Fl f Ar format
.Fl o Ar output_mode
.Op Ar ...
.\" ---------------------------------------------------------------------
.Sh DESCRIPTION
Feeds that are registered for monitoring with
.Xr crossbow-add 1
are individually associated to an output mode that determines how new
.Em items
(stories, articles, etc) are handled by
.Xr crossbow-fetch 1 .
.Pp
The default output mode is called
.Cm print
and consists in printing to
.Xr stdout 3
all the properties of the
.Em item .
The
.Cm pretty
mode also prints on
.Xr stdout 3 ,
but allows the user to define a custom textual
representation in place of the default one.
The last two modes,
.Cm subproc
and
.Cm pipe ,
correspond to two flavours of parametric subprocess execution.
.Pp
All output modes, except for
.Cm print ,
require a format string as an additional parameter.
A format string can contain a number of placeholders, that are expanded
with the properties of each handled
.Em item .
The syntax of placeholders resembles the
.Xr printf 3
function, with some differences and simplifications:
.Bl -bullet
.It
The
.Qq %
sign can only be followed by one or more alphabetic characters.
There is no support for flags, field width or other modifiers.
.It
A raw
.Qq %
sign is represented with
.Qq \e%
(and not as
.Qo
%%
.Qc ).
.It
The backslash character
.Qq \e
is interpreted as escape symbol, enabling the literal
interpretation of the subsequent character.
Notable cases are:
.Bl -dash
.It
The
.Qq \en
sequence is interpreted as a new line.
.It
The
.Qq \e:
sequence is interpreted as a
.Em zero-width break .
.It
The
.Qq \e\e
sequence is interpreted as a literal backspace.
.It
The
.Qq \e\ \&
sequence is interpreted as space character in
.Cm subproc
and
.Cm pipe
mode, while a non-escaped whitespace is be interpreted as a
separator.
An escaped whitespace has no effect in
.Cm pretty
mode, where spaces are maintained verbatim.
.El
.El
.\" ---------------------------------------------------------------------
.Ss Format string expansion
The interpretation of the format string depends on the output mode.
.Pp
If the output mode is
.Cm pretty ,
the format string is treated as a template for the textual
representation of each
.Em item .
The placeholders are expanded with the string representation
of the corresponding properties (listed below), and the resulting string
is printed to
.Xr stdout 3 .
.Pp
If the output mode is
.Cm subproc
or
.Cm pipe ,
the format string is split on white-space into an array of
tokens, and the placeholders are expanded for each of them.
The obtained array is interpreted as a command line, with
the first token being the command to execute, and the
subsequent being the arguments.
For security reasons the command line is not parsed by a shell interpeter.
.Pp
Quotation is currently not supported, thus literal
white-spaces must be individually escaped by backslash
characters
.Po Qo \e Qc Pc .
.Pp
Please mind security when using the
.Cm subproc
and
.Cm pipe
modes.
The invocation of a shell interpreter as subcommand is
especially discouraged, as a specially crafted feed item
might result in remote command execution.
.\" ---------------------------------------------------------------------
.Ss Supported Placeholders
Follows a list of the supported placeholders, and the corresponding fields
of a
.Sy struct mrss_item_t
data type
.Po see
.Lb libmrss
.Pc .
Each field correspond to one property of a feed
.Em item :
.Pp
.Bl -tag -width XXXXX -offset indent -compact
.It %a
author
.It %am
author_email
.It %au
author_uri
.It %cm
contributor_email
.It %co
contributor
.It %cm
contributor_email
.It %cu
contributor_uri
.It %cr
copyright
.It %ct
copyright_type
.It %d
description
.It %dt
description_type
.It %en
enclosure
.It %el
enclosure_length
.It %et
enclosure_type
.It %eu
enclosure_url
.It %g
guid
.It %gp
guid_isPermaLink
.It %l
link
.It %pd
pubDate
.It %sr
source
.It %su
source_url
.It %t
title
.It %tt
title_type
.El
.Pp
The following extra placeholders are also available for each
.Em item :
.Bl -tag -width XXXXX -offset indent
.It %fi
The string identifying, in
.Xr crossbow 1 ,
the feed this item belongs to.
.It %ft
The feed title, as reported by the feed XML.
It corresponds to is the
.Sy title
field of the
.Sy struct mrss_t
data type.
.Po see
.Lb libmrss
.Pc .
.It %n
A per-feed six digit incremental number.
This value is set to zero when the feed is initially registered via
.Xr crossbow-set 1 ,
and gets incremented by one for every new feed item.
.Pp
This is an important security feature: the value of this number is not
controlled by the feed, thus it can be used safely as filename.
See
.Xr crossbow-cookbook 7 .
.Pp
The value is padded with zero to make lexicographical order trivial.
Increment happens even when the execution of a subprocess fails, so that
the same value is never used twice.
.El
.\" ---------------------------------------------------------------------
.Ss Use of the zero-width break
The language recognized by the output format parser allows
the placeholders to be composed by multiple characters.
While this feature makes it easier to have mnemonic placeholders
(such as
.Qq %a
for
.Qq Author
and
.Qq %am
for
.Qq Author eMail Ns
),
it introduces some additional edge cases.
.Pp
The zero-width break sequence
.Pq Qq \e:
has been introduced to cover a case of ambiguity which can
be easily explained by means of an example.
.Pp
Let's consider the case in which both
.Qq %x
and
.Qq %xn
are valid placeholders.
In such case it would get impossible to express the expansion of
.Qq %x
followed by a literal
.Qq n ,
as the sequence
.Qq %x\en
would be rendered as the expansion of
.Qq %x
followed by a new-line, while
.Qq %xn
would be rendered as the expansion of
.Qq %xn .
.Pp
The behaviour is summarized by the following table, where
.Sy expand(x)
expresses the expansion of the
.Qq %x
placeholder into the string representation of the
corresponding
.Em field ,
and the
.Qo . Qc
operator expresses string concatenation.
.Pp
.Bl -tag -width "Format String" -offset indent -compact
.It Em Format String
.Em Expansion
.It Qq %x
expand(x)
.It Qq %xn
expand(xn)
.It Qq %x\em
expand(x) . "m"
.It Qq %x\en
expand(x) . "\en"
.It Qq %x\e:n
expand(x) . "n"
.El
.\" ---------------------------------------------------------------------
.Ss Testing the expansion
By supplying
.Xr crossbow-fetch 1
with the
.Fl d
flag, it is possible to test the configuration to work as
intended without marking new items as seen.
The
.Fl D
flag can be used simultaneously to prevent the execution of sub-commands
in case the feed is configured with the
.Cm subproc
or
.Cm pipe
output modes.
.Pp
When verifying the correctness of a feed configuration, it might be useful
to restrict the fetch operation to the such feed by means of the
.Fl i
flag:
.Bd -literal -offset XXXXXX
$ crossbow fetch -i some-feed
.Ed
.\" ---------------------------------------------------------------------
.Sh EXAMPLES
See
.Xr crossbow-cookbook 7
for a collection of short recipes.
.\" ---------------------------------------------------------------------
.Sh SEE ALSO
.Xr crossbow 1 ,
.Xr crossbow-del 1 ,
.Xr crossbow-fetch 1 ,
.Xr crossbow-query 1 ,
.Xr crossbow-set 1
.\" ---------------------------------------------------------------------
.Sh AUTHORS
.An Giovanni Simoni Aq Mt dacav@fastmail.com
