/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "unittest.h"
#include "util.h"

#include <err.h>
#include <syslog.h>
#include <stdio.h>
#include <stdlib.h>

enum
{
    WIDTH = 60,
};

int main(int argc, char **argv)
{
    int exit_status;
    const struct test *test;

    exit_status = EXIT_SUCCESS;
    for (test = list_test(); test->name; ++test) {
        util_ctx_t ctx;
        int result;
        char test_repr[WIDTH];

        if (!test->enabled) {
            fprintf(stderr, "%-*s [SKIP]\n", WIDTH, test->name);
            continue;
        }

        ctx = util_test_setup(test->name);
        if (!ctx)
            err(1, "util_test_setup");

        snprintf(test_repr, sizeof(test_repr),
                 test->opaque ? "%s(%s)" : "%s",
                 test->name, test->opaque_repr);

        result = test->callback(ctx, test->opaque);
        if (result == 0) {
            fprintf(stderr, "%-*s [ OK ]\n", WIDTH, test_repr);
        } else {
            fprintf(stderr, "%-*s [FAIL, %d]\n", WIDTH, test_repr, result);
            exit_status = EXIT_FAILURE;
        }
        util_test_teardown(ctx);
    }

    return exit_status;
}
