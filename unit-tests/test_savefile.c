/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "savefile.h"

#include "util.h"
#include "str.h"
#include "test_counters.h"
#include "unittest.h"

static int mksavefile(util_ctx_t ctx, savefile_t **sf)
{
    int d;

    d = util_get_tempdir(ctx);
    EXPECT(d != -1);
    EXPECT(*sf = savefile_new(d));

    return 0;
}

static int savefile_base(util_ctx_t ctx, intptr_t opaque)
{
    savefile_t *sf;
    feed_t *f1, *f2;

    EXPECT(mksavefile(ctx, &sf) == 0);
    EXPECT(f1 = savefile_get_feed(sf, __func__));
    EXPECT(f2 = savefile_get_feed(sf, __func__));
    EXPECT(f1 == f2);

    savefile_free(sf);

    return 0;
}

static int loop_entries(util_ctx_t ctx, intptr_t opaque)
{
    /* Insert a collection of items, save, open again, and verify that all
     * the items are stored.  Piggybacks a check for correct storage of
     * the 'items_count' field.
     */

    enum
    {
        n_items = 10,
        items_count = 42,
    };

    savefile_t *sf;
    feed_t *f;
    str_t items[n_items];

    EXPECT(mksavefile(ctx, &sf) == 0);

    EXPECT(f = savefile_get_feed(sf, "feed_name"));

    for (int i = 0; i < n_items; ++i)
        items[i] = STR_DEFINE("item_id");

    EXPECT(feed_set_items(f,
        &(feed_items_t){
            .items = items,
            .n_items = n_items
        }
    ) == 0);
    feed_set_items_count(f, items_count);
    EXPECT(feed_persist(f) != -1);

    savefile_free(sf);

    struct {
        int feeds, items;
    } count = {};
    void *aux = NULL;

    EXPECT(mksavefile(ctx, &sf) == 0);

    /* Iterating over feeds now won't yiedl results, as savefile_load_all
     * must be called first. */
    EXPECT(savefile_iter_feeds(sf, &aux) == NULL);

    /* Scan is executed once */
    EXPECT(test_read_counter("save_scan_called") == 0);
    EXPECT(savefile_load_all(sf) == 0);
    EXPECT(test_read_counter("save_scan_called") == 1);
    EXPECT(savefile_load_all(sf) == 0);
    EXPECT(test_read_counter("save_scan_called") == 1);

    aux = NULL;
    while (f = savefile_iter_feeds(sf, &aux), f) {
        feed_items_t items = feed_get_items(f);

        for (unsigned i = 0; i < items.n_items; ++i) {
            EXPECT(items.items[i].len == strlen("item_id"));
            EXPECT(strncmp(items.items[i].bytes, "item_id", items.items[i].len) == 0);
            count.items++;
        }
        count.feeds++;
    }

    EXPECT(
        feed_get_items_count(
            savefile_get_feed(sf, "feed_name")
        ) == items_count
    );

    EXPECT(count.feeds == 1);
    EXPECT(count.items == n_items);

    savefile_free(sf);
    return 0;
}

static int prepare_scenario(util_ctx_t ctx, savefile_t **sf)
{
    int tmpdir, fd;

    tmpdir = util_get_tempdir(ctx);
    EXPECT(tmpdir != -1);

    #define write_const(bytes) \
        EXPECT(write(fd, (bytes), sizeof(bytes)) == sizeof(bytes))

    /* One unrecognized file */
    fd = openat(tmpdir, "unrecognized", O_CREAT|O_WRONLY, S_IRUSR|S_IWUSR);
    EXPECT(fd != -1);
    write_const("arbitrary content");
    EXPECT(close(fd) != -1);

    /* One incompatible file */
    fd = openat(tmpdir, "incompatible", O_CREAT|O_WRONLY, S_IRUSR|S_IWUSR);
    EXPECT(fd != -1);
    write_const("crossbow\x00\xff\xff future version");
    EXPECT(close(fd) != -1);

    EXPECT(*sf = savefile_new(tmpdir));
    return 0;
}

static int count_listed(const savefile_t *sf)
{
    int listed = 0;

    for (void *aux = NULL; savefile_citer_feeds(sf, &aux); ++listed);

    return listed;
}

static int untouchable_files_skipped(util_ctx_t ctx, intptr_t opaque)
{
    /* Making sure that files which are unrecognized, or recognized to be
     * incompatible, will not be presented during iteration. */

    savefile_t *sf;

    EXPECT(prepare_scenario(ctx, &sf) == 0);

    EXPECT(test_read_counter("added_feeds") == 0);
    EXPECT(savefile_get_feed(sf, "legit") != NULL);
    EXPECT(test_read_counter("added_feeds") == 1);
    EXPECT(savefile_load_all(sf) == 0);
    EXPECT(test_read_counter("added_feeds") == 3);

    EXPECT(count_listed(sf) == 1);

    savefile_free(sf);
    return 0;
}

static int untouchable_files_not_usable(util_ctx_t ctx, intptr_t opaque)
{
    /* Making sure that files which are unrecognized, or recognized to be
     * incompatible, can not be handled even if explicitly loaded. */

    savefile_t *sf;

    EXPECT(prepare_scenario(ctx, &sf) == 0);

    EXPECT(savefile_get_feed(sf, "legit") != NULL);

    savefile_reset_error(sf);
    EXPECT(savefile_get_feed(sf, "incompatible") == NULL);
    EXPECT(savefile_get_error(sf).error == sfe_protected_file);

    savefile_reset_error(sf);
    EXPECT(savefile_get_feed(sf, "unrecognized") == NULL);
    EXPECT(savefile_get_error(sf).error == sfe_protected_file);

    savefile_free(sf);
    return 0;
}

static int corrupt_savefile(util_ctx_t ctx, const char *filename)
{
    int d, f;
    struct stat statbuf;

    d = util_get_tempdir(ctx);
    EXPECT(d != -1);

    f = openat(d, filename, O_WRONLY);
    EXPECT(f != -1);

    EXPECT(fstat(f, &statbuf) != -1);
    EXPECT(statbuf.st_size > 0);
    EXPECT(ftruncate(f, statbuf.st_size - 1) != -1);

    close(f);
    close(d);
    return 0;
}

static int corruption_test(util_ctx_t ctx, intptr_t opaque)
{
    /* Making sure that a recognized and compatbile file which happens to
     * be corrupt cannot be loaded, but will be written over. */

    savefile_t *sf;
    feed_t *f;

    EXPECT(mksavefile(ctx, &sf) == 0);
    EXPECT(f = savefile_get_feed(sf, "hello"));
    EXPECT(test_read_counter("needs_write") == 0);
    EXPECT(feed_set_outfmt(f, &STR_DEFINE("123")) == 0);
    EXPECT(test_read_counter("needs_write") == 1);
    EXPECT(feed_persist(f) != -1);
    savefile_free(sf);

    /* Corrupting the file while preserving the magic number */
    EXPECT(corrupt_savefile(ctx, "hello") != -1);

    /* Loading the file, we find it corrupt */
    EXPECT(mksavefile(ctx, &sf) == 0);
    EXPECT(f = savefile_get_feed(sf, "hello"));
    EXPECT(feed_exists(f));
    EXPECT(test_read_counter("flag_corrupt") == 1);

    /* We can set it with a value and save it. */
    EXPECT(test_read_counter("needs_write") == 1);
    EXPECT(feed_set_outfmt(f, &STR_DEFINE("123")) == 0);
    EXPECT(test_read_counter("needs_write") == 2);
    EXPECT(feed_persist(f) != -1);
    savefile_free(sf);

    /* File is no longer corrupt (flag_corrupt not incremented) */
    EXPECT(mksavefile(ctx, &sf) == 0);
    EXPECT(f = savefile_get_feed(sf, "hello"));
    EXPECT(feed_exists(f));
    EXPECT(feed_set_outfmt(f, &STR_DEFINE("123")) == 0);
    EXPECT(test_read_counter("flag_corrupt") == 1);
    savefile_free(sf);

    return 0;
}

static int add_scan_add_scan(util_ctx_t ctx, intptr_t opaque)
{
    /* Ensure that feeds are stored uniquely in a savefile, and that
     * alternating scans and additions do not result in repeated entries */

    savefile_t *sf;
    feed_t *f;

    /* Prepare scenario by adding a feed.  The addition of an item
     * tiggers an actual write */
    EXPECT(mksavefile(ctx, &sf) == 0);
    EXPECT(f = savefile_get_feed(sf, "hello"));
    EXPECT(feed_set_outfmt(f, &STR_DEFINE("123")) == 0);
    EXPECT(feed_persist(f) != -1);
    savefile_free(sf);

    /* Adding manually the feed should increase the number of iterable
     * feeds.  A subsequent savefile_load_all invocation should not
     * increase the iteration length */
    EXPECT(mksavefile(ctx, &sf) == 0);
    EXPECT(savefile_get_feed(sf, "hello"));
    EXPECT(count_listed(sf) == 1);
    EXPECT(savefile_load_all(sf) == 0);
    EXPECT(count_listed(sf) == 1);
    savefile_free(sf);

    /* Opposite test: loading via savefile_load_all and then adding
     * manually a feed having an existing name sill not increase the
     * number of iterated items.  Adding a new feed will instead do. */
    EXPECT(mksavefile(ctx, &sf) == 0);
    EXPECT(savefile_load_all(sf) == 0);
    EXPECT(count_listed(sf) == 1);
    EXPECT(savefile_get_feed(sf, "hello"));
    EXPECT(count_listed(sf) == 1);
    EXPECT(f = savefile_get_feed(sf, "world"));
    EXPECT(count_listed(sf) == 2);
    EXPECT(feed_set_outfmt(f, &STR_DEFINE("123")) == 0);
    EXPECT(feed_persist(f) != -1);
    savefile_free(sf);

    /* Final check: the number is persistent across saves */
    EXPECT(mksavefile(ctx, &sf) == 0);
    EXPECT(savefile_load_all(sf) == 0);
    EXPECT(count_listed(sf) == 2);
    savefile_free(sf);

    return 0;
}

typedef struct {
    int (*setter)(feed_t *, str_t *);
    str_t (*getter)(const feed_t *);
} set_field_test_t;

static int set_field(util_ctx_t ctx, intptr_t opaque)
{
    /* Ensure the fields is setted and loaded correctly.  Set a field,
     * save, load, check the field */

    savefile_t *sf;
    feed_t *f;
    const set_field_test_t *test = (const set_field_test_t *)opaque;

    EXPECT(mksavefile(ctx, &sf) == 0);
    EXPECT(f = savefile_get_feed(sf, __func__));
    test->setter(f, &STR_DEFINE("value"));
    EXPECT(feed_persist(f) != -1);
    savefile_free(sf);

    EXPECT(mksavefile(ctx, &sf) == 0);
    EXPECT(f = savefile_get_feed(sf, __func__));
    str_t value = test->getter(f);
    EXPECT(str_cmp(&value, &STR_DEFINE("value")) == 0);
    savefile_free(sf);

    return 0;
}

static int save_only_if_needed_1(util_ctx_t ctx, intptr_t opaque)
{
    /* Setting a feed property will enable the 'needs_write' flag.
     * If the new value of the property does not need to be changed, there
     * will be no need to write.
     */

    savefile_t *sf;
    feed_t *f;

    EXPECT(mksavefile(ctx, &sf) == 0);
    EXPECT(f = savefile_get_feed(sf, __func__));
    EXPECT(test_read_counter("needs_write") == 0);

    /* Initially empty, so the new value differs: this will enable the
     * flag */
    feed_set_effective_url(f, &STR_DEFINE("detective Balsam"));
    EXPECT(test_read_counter("needs_write") == 1);

    /* Writing the same value will not enable the flag */
    feed_set_effective_url(f, &STR_DEFINE("detective Balsam"));
    EXPECT(test_read_counter("needs_write") == 1);

    /* Writing a different value will enable the flag */
    feed_set_effective_url(f, &STR_DEFINE("detective Gadget"));
    EXPECT(test_read_counter("needs_write") == 2);

    EXPECT(feed_persist(f) != -1);
    savefile_free(sf);

    EXPECT(mksavefile(ctx, &sf) == 0);
    EXPECT(f = savefile_get_feed(sf, __func__));
    EXPECT(test_read_counter("needs_write") == 2);

    /* The value was preserved, so writing the same will not enable the
     * flag */
    feed_set_effective_url(f, &STR_DEFINE("detective Gadget"));
    EXPECT(test_read_counter("needs_write") == 2);
    savefile_free(sf);

    return 0;
}

static int save_only_if_needed_2(util_ctx_t ctx, intptr_t opaque)
{
    /* Same as save_only_if_needed_1, but with flags. */
    savefile_t *sf;
    feed_t *f;

    EXPECT(mksavefile(ctx, &sf) == 0);
    EXPECT(f = savefile_get_feed(sf, __func__));
    EXPECT(test_read_counter("needs_write") == 0);

    /* Initially empty, so the new value differs: this will enable the
     * flag */
    feed_set_url_type(f, feed_ut_remote);
    EXPECT(test_read_counter("needs_write") == 1);

    /* Writing the same value will not enable the flag */
    feed_set_url_type(f, feed_ut_remote);
    EXPECT(test_read_counter("needs_write") == 1);

    /* Writing a different value will enable the flag */
    feed_set_url_type(f, feed_ut_local);
    EXPECT(test_read_counter("needs_write") == 2);

    EXPECT(feed_persist(f) != -1);
    savefile_free(sf);

    EXPECT(mksavefile(ctx, &sf) == 0);
    EXPECT(f = savefile_get_feed(sf, __func__));
    EXPECT(test_read_counter("needs_write") == 2);

    /* The value was preserved, so writing the same will not enable the
     * flag */
    feed_set_url_type(f, feed_ut_local);
    EXPECT(test_read_counter("needs_write") == 2);
    savefile_free(sf);

    return 0;
}

static int save_only_if_needed_3(util_ctx_t ctx, intptr_t opaque)
{
    /* As the savefile is actually a directory where individual feeds have
     * their own file, this is checking that a file is written only if it
     * is needed, and that this does not imply writing other files. */
    savefile_t *sf;
    feed_t *f1, *f2, *f3;

    EXPECT(mksavefile(ctx, &sf) == 0);

    /* We change the 'f1' feed twice, with different values, so the
     * 'needs_write' flag is set multiple times. */
    EXPECT(f1 = savefile_get_feed(sf, "1"));
    EXPECT(test_read_counter("needs_write") == 0);
    feed_set_url_type(f1, feed_ut_remote);
    EXPECT(test_read_counter("needs_write") == 1);
    feed_set_url_type(f1, feed_ut_local);
    EXPECT(test_read_counter("needs_write") == 2);
    feed_set_url_type(f1, feed_ut_remote);
    EXPECT(test_read_counter("needs_write") == 3);

    /* We change the 'f2' feed once, which is also enabling the
     * 'needs_write' flag, this time for 'f3'.  The test_counter is the
     * same, though. */
    EXPECT(f2 = savefile_get_feed(sf, "2"));
    feed_set_url_type(f2, feed_ut_remote);
    EXPECT(test_read_counter("needs_write") == 4);

    /* The third feed is opened and not really modified: we just set the
     * same value, so there should be no actual modification. */
    EXPECT(f3 = savefile_get_feed(sf, "3"));
    feed_set_url_type(f3, feed_get_url_type(f3));
    EXPECT(test_read_counter("needs_write") == 4);

    /* Two files out of three need to be written.  */
    EXPECT(test_read_counter("feed_written") == 0);
    EXPECT(feed_persist(f1) != -1);
    EXPECT(test_read_counter("feed_written") == 1);
    EXPECT(feed_persist(f3) != -1);
    EXPECT(test_read_counter("feed_written") == 1);
    EXPECT(feed_persist(f2) != -1);
    EXPECT(test_read_counter("feed_written") == 2);

    savefile_free(sf);

    return 0;
}

static int write_delete_write(util_ctx_t ctx, intptr_t opaque)
{
    /* Making sure that writing twice has no effect, unless there's a
     * deletion in between.
     */

    savefile_t *sf;
    feed_t *f;

    EXPECT(mksavefile(ctx, &sf) == 0);

    EXPECT(f = savefile_get_feed(sf, "hello"));
    feed_set_url_type(f, feed_ut_remote);
    EXPECT(feed_persist(f) != -1);
    EXPECT(test_read_counter("feed_written") == 1);

    /* Writing twice has no effect. */
    EXPECT(feed_persist(f) != -1);
    EXPECT(test_read_counter("feed_written") == 1);

    /* Unlink the file */
    feed_mark_deleted(f);
    EXPECT(feed_persist(f) != -1);
    EXPECT(test_read_counter("feed_unlinked") == 1);

    /* Writing after unlink does have effect */
    EXPECT(feed_persist(f) != -1);
    EXPECT(test_read_counter("feed_written") == 2);

    savefile_free(sf);
    return 0;
}

const struct test * list_test(void)
{
    static const set_field_test_t set_field_tests[] = {
        {
            .setter = feed_set_provided_url,
            .getter = feed_get_provided_url,
        }, {
            .setter = feed_set_effective_url,
            .getter = feed_get_effective_url,
        }, {
            .setter = feed_set_outfmt,
            .getter = feed_get_outfmt,
        }
    };

    static const struct test tests[] = {
        TEST(1, savefile_base, NULL),
        TEST(1, loop_entries, NULL),
        TEST(1, untouchable_files_skipped, NULL),
        TEST(1, untouchable_files_not_usable, NULL),
        TEST(1, corruption_test, NULL),
        TEST(1, add_scan_add_scan, NULL),
        TEST(1, set_field, &set_field_tests[0]),
        TEST(1, set_field, &set_field_tests[1]),
        TEST(1, set_field, &set_field_tests[2]),
        TEST(1, save_only_if_needed_1, NULL),
        TEST(1, save_only_if_needed_2, NULL),
        TEST(1, save_only_if_needed_3, NULL),
        TEST(1, write_delete_write, NULL),
        END_TESTS
    };

    return tests;
}
