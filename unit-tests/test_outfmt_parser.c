/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <err.h>
#include <string.h>

#include "outfmt_parser.h"

#include "util.h"
#include "unittest.h"

#if 0
#define DEBUG_P(fmt, ...) warnx(__FILE__ ":%d " fmt, __LINE__, __VA_ARGS__)
#else
#define DEBUG_P(...)
#endif

enum consts {
    /* remember to leave space for terminator (having .begin == NULL) */
    max_emitted = 10,
};

struct test_context {
    ofp_token_t emitted[max_emitted];
    int n_emitted;
    ofp_fail_t fail_reason;
};

struct test_params {
    const char *to_parse;
    ofp_fail_t exp_fail;
    const ofp_token_t exp_toks[max_emitted];
};

static void fail(void *o, ofp_fail_t reason)
{
    struct test_context *tx = o;
    tx->fail_reason = reason;
}

static int emit(void *o, const ofp_token_t *tok)
{
    struct test_context *tx = o;
    int used;
    ofp_token_t *slot;

    if (tx->n_emitted == max_emitted) {
        warnx("max_emitted reached");
        return -1;
    }

    slot = &tx->emitted[tx->n_emitted++];
    if (tok->atom == ofp_atom_pholder) {
        *slot = (ofp_token_t){
            .atom = ofp_atom_pholder,
            .begin = tok->begin + 1,
            .len = 1,
        };
        used = 2;
    } else {
        *slot = *tok;
        used = tok->len;
    }

    DEBUG_P("%d EMIT %s[%.*s]", tx->n_emitted, ofp_atom_str(tok->atom), used, tok->begin);
    return used;
}

static int test_parser(util_ctx_t ctx, intptr_t opaque)
{
    struct test_context tx = {};
    const struct test_params *tp = (struct test_params *)opaque;
    int e;

    struct ofp_setup ofps = {
        .emit = emit,
        .fail = fail,
        .opaque = &tx,
    };

    DEBUG_P("scan(%s)", tp->to_parse);
    e = ofp_scan(&ofps, tp->to_parse, strlen(tp->to_parse));
    DEBUG_P("tx.fail_reason=%s tp->exp_fail=%s",
            ofp_fail_str(tx.fail_reason),
            ofp_fail_str(tp->exp_fail));
    EXPECT(tx.fail_reason == tp->exp_fail);
    EXPECT(tx.fail_reason == ofp_fail_none || e != 0);

    for (int i = 0; i <= tx.n_emitted && tp->exp_toks[i].begin; ++i) {
        const ofp_token_t *exp = &tp->exp_toks[i];
        const ofp_token_t *item = &tx.emitted[i];

        DEBUG_P("%02d) expected: atom=%s len=%zu content=[%.*s]",
                i, ofp_atom_str(exp->atom), exp->len, (int)exp->len, exp->begin);
        DEBUG_P("    obtained: atom=%s len=%zu content=[%.*s]",
                ofp_atom_str(item->atom), item->len, (int)item->len, item->begin);
        EXPECT(exp->atom == item->atom);
        EXPECT(exp->len == item->len);
        EXPECT(strncmp(exp->begin, item->begin, exp->len) == 0);
    }

    return 0;
}

#define E(a, x) (ofp_token_t){ \
    .atom = (a), \
    .begin = (x), \
    .len = (sizeof(x) - 1), \
}

#define emitting(...) (ofp_token_t[]){ __VA_ARGS__, {}}

const struct test * list_test(void)
{
    static struct test_params one_char[] = {
        [ 0].to_parse = "x",
        [ 0].exp_toks[0] = E(ofp_atom_verbatim, "x"),
        [ 1].to_parse = " ",
        [ 1].exp_toks[0] = E(ofp_atom_whitespace, " "),
        [ 2].to_parse = "%",
        [ 2].exp_fail = ofp_fail_invalid_pholder,
        [ 3].to_parse = "\\",
        [ 3].exp_fail = ofp_fail_trail_escape,
    };

    static struct test_params two_chars[] = {
        [ 0].to_parse = "xy",
        [ 0].exp_toks[0] = E(ofp_atom_verbatim, "xy"),
        [ 1].to_parse = "x ",
        [ 1].exp_toks[0] = E(ofp_atom_verbatim, "x"),
        [ 1].exp_toks[1] = E(ofp_atom_whitespace, " "),
        [ 2].to_parse = "x%",
        [ 2].exp_fail = ofp_fail_invalid_pholder,
        [ 3].to_parse = "x\\",
        [ 3].exp_fail = ofp_fail_trail_escape,

        [ 4].to_parse = " y",
        [ 4].exp_toks[0] = E(ofp_atom_whitespace, " "),
        [ 4].exp_toks[1] = E(ofp_atom_verbatim, "y"),
        [ 5].to_parse = "  ",
        [ 5].exp_toks[0] = E(ofp_atom_whitespace, "  "),
        [ 6].to_parse = " %",
        [ 6].exp_fail = ofp_fail_invalid_pholder,
        [ 7].to_parse = " \\",
        [ 7].exp_fail = ofp_fail_trail_escape,

        [ 8].to_parse = "%x",
        [ 8].exp_toks[0] = E(ofp_atom_pholder, "x"),
        [ 9].to_parse = "% ",
        [ 9].exp_fail = ofp_fail_invalid_pholder,
        [10].to_parse = "%\\",
        [10].exp_fail = ofp_fail_trail_escape,
        [11].to_parse = "%%",
        [11].exp_fail = ofp_fail_invalid_pholder,

        [12].to_parse = "\\x",
        [12].exp_toks[0] = E(ofp_atom_verbatim, "\\x"),
        [13].to_parse = "\\ ",
        [13].exp_toks[0] = E(ofp_atom_verbatim, "\\ "),
        [14].to_parse = "\\\\",
        [14].exp_toks[0] = E(ofp_atom_verbatim, "\\\\"),
        [15].to_parse = "\\%",
        [15].exp_toks[0] = E(ofp_atom_verbatim, "\\%"),
    };

    static struct test_params edges[] = {
        [ 0].to_parse = "x%yz",
        [ 0].exp_toks[0] = E(ofp_atom_verbatim, "x"),
        [ 0].exp_toks[1] = E(ofp_atom_pholder, "y"),
        [ 0].exp_toks[2] = E(ofp_atom_verbatim, "z"),
        [ 1].to_parse = " x%s",
        [ 1].exp_toks[0] = E(ofp_atom_whitespace, " "),
        [ 1].exp_toks[1] = E(ofp_atom_verbatim, "x"),
        [ 1].exp_toks[2] = E(ofp_atom_pholder, "s"),
        [ 2].to_parse = " %sx",
        [ 2].exp_toks[0] = E(ofp_atom_whitespace, " "),
        [ 2].exp_toks[1] = E(ofp_atom_pholder, "s"),
        [ 2].exp_toks[2] = E(ofp_atom_verbatim, "x"),
        [ 3].to_parse = "hello \\%x %t world %a\n",
        [ 3].exp_toks[0] = E(ofp_atom_verbatim, "hello"),
        [ 3].exp_toks[1] = E(ofp_atom_whitespace, " "),
        [ 3].exp_toks[2] = E(ofp_atom_verbatim, "\\%x"),
        [ 3].exp_toks[3] = E(ofp_atom_whitespace, " "),
        [ 3].exp_toks[4] = E(ofp_atom_pholder, "t"),
        [ 3].exp_toks[5] = E(ofp_atom_whitespace, " "),
        [ 4].exp_toks[6] = E(ofp_atom_verbatim, "world"),
        [ 3].exp_toks[7] = E(ofp_atom_whitespace, " "),
        [ 3].exp_toks[8] = E(ofp_atom_pholder, "a"),
        [ 3].exp_toks[9] = E(ofp_atom_verbatim, "\n"),
        [ 4].to_parse = "\%x",
        [ 4].exp_toks[1] = E(ofp_atom_verbatim, "%x"),
    };

    static struct test tests[] = {
        TEST(1, test_parser, &one_char[0]),
        TEST(1, test_parser, &one_char[1]),
        TEST(1, test_parser, &one_char[2]),
        TEST(1, test_parser, &one_char[3]),
        TEST(1, test_parser, &two_chars[0]),
        TEST(1, test_parser, &two_chars[1]),
        TEST(1, test_parser, &two_chars[2]),
        TEST(1, test_parser, &two_chars[3]),
        TEST(1, test_parser, &two_chars[4]),
        TEST(1, test_parser, &two_chars[5]),
        TEST(1, test_parser, &two_chars[6]),
        TEST(1, test_parser, &two_chars[7]),
        TEST(1, test_parser, &two_chars[8]),
        TEST(1, test_parser, &two_chars[9]),
        TEST(1, test_parser, &two_chars[10]),
        TEST(1, test_parser, &two_chars[11]),
        TEST(1, test_parser, &two_chars[12]),
        TEST(1, test_parser, &two_chars[13]),
        TEST(1, test_parser, &two_chars[14]),
        TEST(1, test_parser, &two_chars[15]),
        TEST(1, test_parser, &edges[0]),
        TEST(1, test_parser, &edges[1]),
        TEST(1, test_parser, &edges[2]),
        TEST(1, test_parser, &edges[3]),
        TEST(1, test_parser, &edges[4]),
        END_TESTS
    };
    return tests;
}
