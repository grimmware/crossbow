# Crossbow - the minimalist feed aggregator

**crossbow(1)** is a minimalist RSS feed aggregator.

* Written in modern C for UNIX-like systems;

* Complete with manpages;

* Remote feeds can be fetched via HTTP, HTTPS and Gopher;

* Designed to be invoked periodically by cron(8), so
  no background process is needed;

* On a system where local email delivery is configured, the
  output is naturally converted into local emails.  No user
  interfaces is needed, other than the regular command line
  and a local mail user agent, such as mutt(1);

* It can be configured to handle content with external
  commands.  This allows, for example, to automatically
  parse or download new content without user interaction.

**crossbow(1)** is composed by a collection of sub-commands:

 - **crossbow-set(1)**
    Registers a feed for monitoring, or updates the
    configuration for a previously registered feed.

 - **crossbow-fetch(1)**
    Retrieves updates from each monitored feed, processing
    previously unseen items.

 - **crossbow-query(1)**
    Prints information about the the subscribed feeds.

 - **crossbow-del(1)**
    Deletes a URL from the set of monitored feeds.


# Getting started

For compilation and installation see below.

Check out the **crossbow-cookbook(7)** man page for a collection of
examples tackling the most common usage cases.

# Dependencies

      * libcurl
      |
      * libnxml - https://github.com/bakulf/libnxml
      |           https://github.com/dacav/libnxml/releases
      |
      * libmrss - https://github.com/bakulf/libmrss
      |           https://github.com/dacav/libmrss/releases
      |
    * | uthash - https://troydhanson.github.io/uthash/
    |/
    * crossbow

# Releases

Users should rely on the dist tarballs, available for download on
the [Releases](https://gitlab.com/dacav/crossbow/-/releases) page.  Please
follow the installation procedure described in the next sections of this
page.

Revisions checked out from git require the installation of GNU Autotools,
and bootstrapping by means of the `./autogen.sh` script.  Contributors are
advised to install the pre-commit hook available in the `./contrib`
directory.

# Compilation / Installation

Crossbow was successfully compiled and installed on various
operating systems:

* Debian GNU/Linux Bullseye
* Fedora 31
* FreeBSD 12
* OpenBSD 6.5
* MacOS

On some operating systems all the dependencies are already
available as pre-compiled packages, while on others the
dependencies must be built from sources.

## Note:

To date 2020-03-22 I was not able to find dist packages for
libnxml and libmrss, which are both needed as transitive
dependencies.  The tagged revisions available on Github
(user "bakulf") are not accompanied by the corresponding
release archives, and therefore they depend on GNU
Autotools.

For this reason I created the dist packages for both
libraries, and I published them as release:

* https://github.com/dacav/libnxml/releases/download/0.18.3/libnxml-0.18.3.tar.gz

* https://github.com/dacav/libmrss/releases/download/0.19.2/libmrss-0.19.2.tar.gz

Chances are that future versions of Crossbow will change
some of these dependencies, or at least allow the choice of
more popular XML parsers (such as libxml).

I'm getting organized to improve testing automatism under different
systems.  With OpenBSD I need to do a bit more work... but I managed to
build it once on https://tilde.institute

## Generic installation procedure

Once the dependencies have been installed, unpack the
distribution tarball and proceed with the usual installation
commands:

     ./configure && make && make install

## Installation of dependencies:

### On Debian

1. Install the dependencies (all are available from the
   repositories):

       # apt install make gcc pkg-config libmrss0-dev uthash-dev

2. Proceed with the generic installation procedure.

### On Fedora

1. Some dependencies are available from repositories:

        # dnf install libnxml-devel uthash-devel

2. Install libmrss:

        # cd -
        # curl -LO https://github.com/dacav/libmrss/releases/download/0.19.2/libmrss-0.19.2.tar.gz
        # tar xzf libmrss-0.19.2.tar.gz
        # cd libmrss-0.19.2
        # ./configure && make && make install

3. Proceed with the generic installation procedure.

### On FreeBSD

The following procedure was tested on a pristine FreeBSD 12
operating system:

1. Some dependencies are available from repositories:

        # pkg install curl gzip pkgconf uthash

2. Prepare the environment to include dependencies installed
   under /usr/local:

        # setenv LDFLAGS -L/usr/local/lib
        # setenv CPPFLAGS -I/usr/local/include
        # setenv PKG_CONFIG_PATH /usr/local/lib/pkgconfig

3. Install libnxml:

        # curl -LO https://github.com/dacav/libnxml/releases/download/0.18.3/libnxml-0.18.3.tar.gz
        # tar xzf libnxml-0.18.3.tar.gz
        # cd libnxml-0.18.3
        # ./configure && make && make install

4. Install libmrss:

        # cd -
        # curl -LO https://github.com/dacav/libmrss/releases/download/0.19.2/libmrss-0.19.2.tar.gz
        # tar xzf libmrss-0.19.2.tar.gz
        # cd libmrss-0.19.2
        # ./configure && make && make install

5. Proceed with the generic installation procedure.

### On OpenBSD

In short, same as FreeBSD, except uthash should also be
installed manually.  See https://troydhanson.github.io/uthash/

### On MacOS

Up to release 1.1.1, the software won't compile unless the `CFLAGS`
environment variable is assigned with `-D_DARWIN_C_SOURCE`.
This issue has been fixed with version 1.1.2.

libnxml and libmrss are available in brew, while uthash is missing.
